﻿// Decompiled with JetBrains decompiler
// Type: DailyTask.ErrorLog
// Assembly: DailyTask, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 437E0264-3720-4F4D-A3E4-34CE8F4E0540
// Assembly location: C:\Users\hitendra.machhi\Downloads\bin(1)\bin\DailyTask.exe

using System;
using System.Configuration;
using System.IO;
using System.Text;

namespace DailyTask
{
    internal class ErrorLog
    {
        public bool WriteErrorLog(string LogMessage)
        {
            bool flag = false;
           string LogPath = ConfigurationManager.AppSettings["LogDirectory"].ToString();
            DateTime now = DateTime.Now;
            now.ToString();
            this.CheckCreateLogDirectory(LogPath);
            string str = this.BuildLogLine(now, LogMessage);
            string path = LogPath + "Log_" + this.LogFileName(DateTime.Now) + ".txt";
            lock (typeof(ErrorLog))
            {
                StreamWriter streamWriter = (StreamWriter)null;
                try
                {
                    streamWriter = new StreamWriter(path, true);
                    streamWriter.WriteLine(str);
                    flag = true;
                }
                catch
                {
                }
                finally
                {
                    streamWriter?.Close();
                }
            }
            return flag;
        }

        public bool WriteCustomerInfoLog(string custInfo)
        {
            bool flag = false;
            string LogPath = ConfigurationManager.AppSettings["LogDirectory"].ToString();
            DateTime now = DateTime.Now;
            now.ToString();
            this.CheckCreateLogDirectory(LogPath);
            string str = this.BuildLogLine(now, custInfo);
            string path = LogPath + "Log_CustomerInformaion_" + this.LogFileName(DateTime.Now) + ".txt";
            lock (typeof(ErrorLog))
            {
                StreamWriter streamWriter = (StreamWriter)null;
                try
                {
                    streamWriter = new StreamWriter(path, true);
                    streamWriter.WriteLine(str);
                    flag = true;
                }
                catch
                {
                }
                finally
                {
                    streamWriter?.Close();
                }
            }
            return flag;
        }        

        private bool CheckCreateLogDirectory(string LogPath)
        {
            bool flag = false;
            if (new DirectoryInfo(LogPath).Exists)
            {
                flag = true;
            }
            else
            {
                try
                {
                    Directory.CreateDirectory(LogPath);
                    flag = true;
                }
                catch
                {
                }
            }
            return flag;
        }

        private string BuildLogLine(DateTime CurrentDateTime, string LogMessage)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(this.LogFileEntryDateTime(CurrentDateTime));
            stringBuilder.Append("\t");
            stringBuilder.Append(LogMessage);
            return stringBuilder.ToString();
        }

        public string LogFileEntryDateTime(DateTime CurrentDateTime)
        {
            return CurrentDateTime.ToString("MM-dd-yyyy HH:mm:ss");
        }

        private string LogFileName(DateTime CurrentDateTime)
        {
            return CurrentDateTime.ToString("MM_dd_yyyy");
        }
    }
}
