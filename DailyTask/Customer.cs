﻿// Decompiled with JetBrains decompiler
// Type: DailyTask.Customer
// Assembly: DailyTask, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 437E0264-3720-4F4D-A3E4-34CE8F4E0540
// Assembly location: C:\Users\hitendra.machhi\Downloads\bin(1)\bin\DailyTask.exe

using FirebirdSql.Data.FirebirdClient;
using System;
using System.Configuration;
using System.Data;
using System.Net;
using System.Net.Mail;
using MailChimp.Net.Core;
using MailChimp.Net.Models;
using MailChimp.Net.Interfaces;
using MailChimp.Net;
using System.Threading.Tasks;

namespace DailyTask
{
    internal class Customer
    {
        // Instantiate Mail Chimp Manager
        private static IMailChimpManager mailChimpManager = new MailChimpManager(ConfigurationManager.AppSettings["apikey"].ToString());

        public static string listId { get; set; }

        public static string CID { get; set; }

        public static string Email { get; set; }

        public static string Fname { get; set; }

        public static string Lname { get; set; }

        public static string Status { get; set; }

        public static string Location { get; set; }

        public static string GEN { get; set; }

        public static string BDT { get; set; }

        public static DateTime BDT1 { get; set; }

        public static string JDT { get; set; }

        public static DateTime JDT1 { get; set; }

        public static string LDT { get; set; }

        public static DateTime LDT1 { get; set; }

        public static string PNO { get; set; }

        public static string PNO2 { get; set; }

        public static string MNO { get; set; }

        public static string AMAIL { get; set; }

        public static string ASMS { get; set; }

        public static string CNO { get; set; }

        public static string GDES { get; set; }

        public static string CGID { get; set; }

        public static FbConnection fbCon { get; set; }

        public static string apikey { get; set; }

        public static string OldEmail { get; set; }

        public static string INTGROUP { get; set; }

        public static string BDSTORE { get; set; }

        public static string BD { get; set; }

        public static DateTime BD1 { get; set; }

        private static void Main(string[] args)
        {
            try
            {
                string str = Customer.Mailchimp();
                Console.WriteLine(" " + str);
                new ErrorLog().WriteErrorLog(str.ToString() ?? "");
            }
            catch (Exception ex)
            {
                new ErrorLog().WriteErrorLog(ex.ToString() ?? "");
            }
        }

        public static string Mailchimp()
        {
            try
            {
                Customer.apikey = ConfigurationManager.AppSettings["apikey"].ToString();
                Customer.listId = ConfigurationManager.AppSettings["ListId"].ToString();
                string str = ConfigurationManager.ConnectionStrings["Mycon"].ConnectionString.ToString();

                string selectCommandText = "SELECT C.BICUSTOMERID,C.SEMAILADDRESS,C.SMAILINTEGRATIONKEY FROM CUSTOMER C WHERE C.SEMAILADDRESS !=''AND C.SEMAILADDRESS != COALESCE(C.SMAILINTEGRATIONKEY,'') AND C.BLOYALTYMEMBER='T' AND C.BACCEPTMAIL='T'";
                Customer.fbCon = new FbConnection(str.ToString());
                Customer.fbCon.Open();
                FbDataAdapter fbDataAdapter1 = new FbDataAdapter(selectCommandText, Customer.fbCon);
                DataSet dataSet1 = new DataSet();
                fbDataAdapter1.Fill(dataSet1);
                if (dataSet1.Tables[0].Rows.Count == 0)
                    return "Database is Empty";

                for (int index1 = 0; index1 != dataSet1.Tables[0].Rows.Count; ++index1)
                {
                    FbDataAdapter fbDataAdapter2 = new FbDataAdapter("SELECT C.BICUSTOMERID,C.SFIRSTNAMES,C.SSURNAME,C.SEMAILADDRESS,C.ISTATUSID,C.SGENDER,C.DTBIRTHDAY,C.DTJOINED,C.DTLASTVISIT,C.SPHONENUMBER1,C.SPHONENUMBER2,C.SMOBILENUMBER,C.BACCEPTMAIL,C.BACCEPTSMS,C.SMAILINTEGRATIONKEY,L.SDESCRIPTION,CD.BICARDNUMBER,CG.BICUSTOMERGROUPID,DCG.SDESCRIPTION,L.BBRANDEDSTORE FROM CUSTOMER C LEFT JOIN CUSTOMER_EXTRACARDS CD ON CD.BICUSTOMERID=C.BICUSTOMERID LEFT JOIN CUSTOMER_GROUPS CG ON CG.BICUSTOMERID=C.BICUSTOMERID LEFT JOIN DEF_CUSTOMER_GROUP DCG ON DCG.BICUSTOMERGROUPID=CG.BICUSTOMERGROUPID LEFT JOIN LOCATION L ON L.ILOCATIONID=C.ILOCATIONID where C.BICUSTOMERID='" + int.Parse(dataSet1.Tables[0].Rows[index1][0].ToString()).ToString() + "'AND L.BSTOCKLOCATION='T'", Customer.fbCon);
                    DataSet dataSet2 = new DataSet();
                    fbDataAdapter2.Fill(dataSet2);
                    if (dataSet2.Tables[0].Rows.Count != 0)
                    {
                        for (int index2 = 0; index2 != dataSet2.Tables[0].Rows.Count; ++index2)
                        {
                            Customer.CID = dataSet2.Tables[0].Rows[index2][0].ToString().Trim();
                            Customer.Fname = dataSet2.Tables[0].Rows[index2][1].ToString().Trim();
                            Customer.Lname = dataSet2.Tables[0].Rows[index2][2].ToString().Trim();
                            Customer.Email = dataSet2.Tables[0].Rows[index2][3].ToString().Trim();
                            Customer.Status = dataSet2.Tables[0].Rows[index2][4].ToString().Trim();
                            Customer.GEN = dataSet2.Tables[0].Rows[index2][5].ToString().Trim();
                            if (dataSet2.Tables[0].Rows[index2][6].ToString().Trim() != "" && dataSet2.Tables[0].Rows[index2][6].ToString().Trim() != null)
                            {
                                Customer.BDT1 = Convert.ToDateTime(dataSet2.Tables[0].Rows[index2][6].ToString().Trim());
                                Customer.BDT = Convert.ToDateTime(Customer.BDT1).ToString("MM/dd/yyyy");
                                Customer.BD = Convert.ToDateTime(Customer.BDT1).ToString("MM/dd");
                            }
                            else
                            {
                                Customer.BDT = "";
                                Customer.BD = "";
                            }
                            if (dataSet2.Tables[0].Rows[index2][7].ToString().Trim() != "" && dataSet2.Tables[0].Rows[index2][7].ToString().Trim() != null)
                            {
                                Customer.JDT1 = Convert.ToDateTime(dataSet2.Tables[0].Rows[index2][7].ToString().Trim());
                                Customer.JDT = Convert.ToDateTime(Customer.JDT1).ToString("MM/dd/yyyy");
                            }
                            else
                                Customer.JDT = "";
                            if (dataSet2.Tables[0].Rows[index2][8].ToString().Trim() != "" && dataSet2.Tables[0].Rows[index2][8].ToString().Trim() != null)
                            {
                                Customer.LDT1 = Convert.ToDateTime(dataSet2.Tables[0].Rows[index2][8].ToString().Trim());
                                Customer.LDT = Convert.ToDateTime(Customer.LDT1).ToString("MM/dd/yyyy");
                            }
                            else
                                Customer.LDT = "";
                            Customer.PNO = dataSet2.Tables[0].Rows[index2][9].ToString().Trim();
                            Customer.PNO2 = dataSet2.Tables[0].Rows[index2][10].ToString().Trim();
                            Customer.MNO = dataSet2.Tables[0].Rows[index2][11].ToString().Trim();
                            Customer.AMAIL = dataSet2.Tables[0].Rows[index2][12].ToString().Trim();
                            Customer.ASMS = dataSet2.Tables[0].Rows[index2][13].ToString().Trim();
                            Customer.OldEmail = dataSet2.Tables[0].Rows[index2][14].ToString().Trim();
                            Customer.Location = dataSet2.Tables[0].Rows[index2][15].ToString().Trim();
                            Customer.CNO = dataSet2.Tables[0].Rows[index2][16].ToString().Trim();
                            Customer.CGID = dataSet2.Tables[0].Rows[index2][17].ToString().Trim();
                            Customer.GDES = dataSet2.Tables[0].Rows[index2][18].ToString().Trim();
                            Customer.BDSTORE = dataSet2.Tables[0].Rows[index2][19].ToString().Trim();
                            switch (Customer.CGID)
                            {
                                case "150000005":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000006":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000007":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000008":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000009":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000010":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000011":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000012":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000013":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000014":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000015":
                                    Customer.INTGROUP += "(WB)HEART-BP-CHOLESTEROL,";
                                    break;
                                case "150000016":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000017":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000018":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000019":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000020":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000021":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000022":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000023":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000024":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000025":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000026":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000027":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000028":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000029":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                case "150000030":
                                    Customer.INTGROUP = Customer.INTGROUP + Customer.GDES.ToString() + ",";
                                    break;
                                default:
                                    Customer.INTGROUP += "NO GROUP,";
                                    break;
                            }                            
                        }
                        Customer.AddorUpdateMember();
                    }
                    Customer.INTGROUP = "";
                }
                return " Record Inserted @ " + DateTime.Now.ToString();
            }
            catch (Exception ex)
            {
                Customer.SendEmail(ex.ToString());
                new ErrorLog().WriteErrorLog(ex.ToString() + "\n");
                return ex.ToString();
            }
            finally
            {
                Customer.fbCon.Close();
            }
        }

        public async static Task AddMember()
        {
            try
            {
                Customer.Status = !(Customer.Status.ToString() == "1") ? "NO" : "YES";
                Customer.AMAIL = !(Customer.AMAIL.ToString() == "T") ? "NO" : "YES";
                Customer.ASMS = !(Customer.ASMS.ToString() == "T") ? "NO" : "YES";
                Customer.BDSTORE = !(Customer.BDSTORE.ToString() == "T") ? "NO" : "YES";

                var member = new Member
                {
                    EmailAddress = Customer.Email,
                    //StatusIfNew = MailChimp.Net.Models.Status.Pending,
                    Status = MailChimp.Net.Models.Status.Subscribed,
                    ListId = Customer.listId
                };

                member.MergeFields.Add("CID", (object)Customer.CID);
                member.MergeFields.Add("FNAME", (object)Customer.Fname);
                member.MergeFields.Add("LNAME", (object)Customer.Lname);
                member.MergeFields.Add("STATUS", (object)Customer.Status);
                member.MergeFields.Add("LOCATION", (object)Customer.Location);
                member.MergeFields.Add("GEN", (object)Customer.GEN);
                member.MergeFields.Add("BDT", (object)Customer.BDT);
                member.MergeFields.Add("JDT", (object)Customer.JDT);
                member.MergeFields.Add("LDT", (object)Customer.LDT);
                member.MergeFields.Add("PNO", (object)Customer.PNO);
                member.MergeFields.Add("PNO2", (object)Customer.PNO2);
                member.MergeFields.Add("MNO", (object)Customer.MNO);
                member.MergeFields.Add("AMAIL", (object)Customer.AMAIL);
                member.MergeFields.Add("ASMS", (object)Customer.ASMS);
                member.MergeFields.Add("CNO", (object)Customer.CNO);
                member.MergeFields.Add("BD", (object)Customer.BD);
                member.MergeFields.Add("INTERESTS", (object)Customer.INTGROUP.ToString().Trim());
                member.MergeFields.Add("BSTORE", (object)Customer.BDSTORE);

                // Add
                new ErrorLog().WriteCustomerInfoLog(Customer.CID + " - " + Customer.Email + " -- Try to add --\n");
                var result = await mailChimpManager.Members.AddOrUpdateAsync(Customer.listId, member);
                new ErrorLog().WriteCustomerInfoLog(Customer.CID + " - " + Customer.Email + " -- Added -- \n");

                if (result != null)
                {
                    Console.WriteLine(result.ToString());
                }
            }
            catch (MailChimpException ex)
            {
                if(ex.Status == 400)
                {
                    new ErrorLog().WriteErrorLog(ex.ToString() + "\n");
                    new ErrorLog().WriteCustomerInfoLog(Customer.CID + " - " + Customer.Email + " -- Not Added -- \n");
                    new ErrorLog().WriteCustomerInfoLog(ex.Message.ToString() + "\n");
                    return;
                }

                new ErrorLog().WriteErrorLog(ex.ToString() + "\n");
                Console.WriteLine(ex.ToString());
            }
            catch(Exception ex)
            {
                new ErrorLog().WriteErrorLog(ex.ToString() + "\n");
                Console.WriteLine(ex.ToString());
            }

            try
            {
                string addedMailAddress = mailChimpManager.Members.GetAsync(Customer.listId, Customer.Email).Result.EmailAddress;
                if (addedMailAddress.Equals(Customer.Email, StringComparison.OrdinalIgnoreCase))
                {
                    string strQry = "update customer set IMAILINTEGRATIONSTATUS=1,SMAILINTEGRATIONKEY=SEMAILADDRESS Where BICUSTOMERID='" + Customer.CID + "'";
                    Customer.UpdateMemberEmail(strQry);
                }
            }
            catch (MailChimpException ex)
            {
                new ErrorLog().WriteErrorLog("An error while fatching -" + Customer.CID + Customer.Email + "\n");
                new ErrorLog().WriteErrorLog(ex.ToString() + "\n");
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                new ErrorLog().WriteErrorLog("An error while fatching  -" + Customer.CID + Customer.Email + "\n");
                new ErrorLog().WriteErrorLog(ex.ToString() + "\n");
                Console.WriteLine(ex.ToString());
            }
        }

        public static void AddorUpdateMember()
        {
            // Get Old Mail
            try
            {
                string oldMail = mailChimpManager.Members.GetAsync(Customer.listId, Customer.OldEmail).Result.EmailAddress;

                Customer.AddMember().Wait();

                if (oldMail != null && !(oldMail.Equals(Customer.Email, StringComparison.OrdinalIgnoreCase)))
                {
                    string strQry = "update customer set SMAILINTEGRATIONKEY=SEMAILADDRESS Where BICUSTOMERID='" + Customer.CID + "'";
                    Customer.UpdateMemberEmail(strQry);
                }
            }
            catch (MailChimpException ex)
            {
                new ErrorLog().WriteErrorLog("An error while fatching " + Customer.OldEmail + "\n");
                new ErrorLog().WriteErrorLog(ex.ToString() + "\n");
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                new ErrorLog().WriteErrorLog("An error while fatching " + Customer.OldEmail + "\n");
                new ErrorLog().WriteErrorLog(ex.ToString() + "\n");
                Console.WriteLine(ex.ToString());
            }
            
        }

        public static void UpdateMemberEmail(string strQuery)
        {
            try
            {
                FbCommand fbCommand = new FbCommand(strQuery, Customer.fbCon);
                FbTransaction fbTransaction = Customer.fbCon.BeginTransaction();
                fbCommand.Transaction = fbTransaction;
                fbCommand.ExecuteNonQuery();
                fbTransaction.Commit();
                new ErrorLog().WriteCustomerInfoLog("Congrats! your details has been updated \n");
                Console.WriteLine("Congrats! your details has been updated");
            }
            catch (Exception ex)
            {
                new ErrorLog().WriteErrorLog("Something is wrong while updating details." + strQuery + "\n");
                new ErrorLog().WriteErrorLog(ex.ToString() + "\n");
                Console.WriteLine("Something is wrong while updating details." + ex.ToString());
            }              
        }

        public static void SendEmail(string body)
        {
            try
            {
                string address1 = ConfigurationManager.AppSettings["emailFromAddress"].ToString();
                string address2 = ConfigurationManager.AppSettings["emailTOAddress"].ToString();
                string userName = ConfigurationManager.AppSettings["ID"].ToString();


                MailMessage message = new MailMessage();
                message.From = new MailAddress(address1);
                message.To.Add(new MailAddress(address2));
                message.Subject = "Capital Chemist Mailchimp Error";
                message.Body = "   " + body;
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.Port = 25;
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = true;
                message.CC.Add("vishnu.baroda@gmail.com");
                smtpClient.Credentials = (ICredentialsByHost)new NetworkCredential(userName, "Vishnu@123");
                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                new ErrorLog().WriteErrorLog(ex.ToString() + "\n");
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
